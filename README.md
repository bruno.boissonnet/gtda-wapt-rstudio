# GTDA-WAPT-RStudio

Paquet [WAPT](https://www.tranquil.it/gerer-parc-informatique/decouvrir-wapt/) pour l'installation silencieuse de RStudio.


## Paquet RStudio

Ce paquet installe la dernière version de RStudio de manière silencieuse.

 
- Dernière version : 2023.06.1+524 (07/07/2023)
- Sources de l’appli : https://docs.posit.co/previous-versions/rstudio/
- Paramètres d’installation silencieuse : /S
- Prérequis : Windows 10/11
- Dépendances : R
- Changelog : https://docs.posit.co/ide/news/
- Spécificités complémentaires d’installation : Supprimer l’icône du bureau public
- Spécificités complémentaires en ce qui concerne les clés de registre : Néant


## Utiliser ce paquet

Pour utiliser ce paquet, il faut : 

1. Télécharger tous les fichiers
2. Ouvrir la console WAPT
3. Générer un modèle de paquet
4. Pointer sur les fichiers téléchargés


## Notes de version

---

### 2023.12.1-402

- Nouveau fichier d'installation : RStudio-2023.12.1-402.exe

#### FICHIER : control

- MODIFICATION : 
```
version           : 2023.12.1-402
```

---

### 2023.06.1+524

- Nouveau fichier d'installation : RStudio-2023.06.1-524.exe

#### FICHIER : control

- MODIFICATION : 
```
version           : 2023.06.1-524
```

#### FICHIER : setup.py

- MODIFICATION : Généralisation du code pour ne plus avoir à le modifier à chaque nouvelle version.



